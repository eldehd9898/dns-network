#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netdb.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#define PORT_NUM 3800
#define MAXLEN 1000

struct map {               
	char *key;
	char *value;
	int hit;             
};

void setting_map(struct map a[],char rmsg[],char *s){
    int i = 0;
    for(i = 0 ; i < MAXLEN ; i++){
	if(strncmp(a[i].key,rmsg,strlen(rmsg)) == 0){
	    a[i].hit = a[i].hit+1;
	    break;
        }else if(strlen(a[i].key)==0){
	    a[i].key = (char*)calloc(strlen(rmsg),sizeof(char));
	    a[i].value = (char*)calloc(strlen(s),sizeof(char));
	    strncpy(a[i].key, rmsg,strlen(rmsg));
	    strncpy(a[i].value, s,strlen(s));
	    break;
	}else if(strlen(a[i].key)!=0) continue;
    }
    
}

int size_check(FILE *fp){
    int size=0;
    int size1=0;
    int size2=0;

    fseek(fp,0,SEEK_END);
    size = ftell(fp);
    fseek(fp,0,SEEK_SET);
    size1 = ftell(fp);
    size2 = size - size1;
    return size2;
}
/*
char* read_dnsinfo(){
    char *buffer;
    int size;
    int count;

    FILE *fp = fopen("DNS_info.dat","r");

    fseek(fp,0,SEEK_END);
    size = ftell(fp);
    buffer = malloc(size +1);
    memset(buffer,0,size+1);

    fseek(fp,0,SEEK_SET);

    fread(buffer,size,1,fp);

    fclose(fp);
    free(buffer); 
    return buffer;
}*/		
void hit_sort(struct map m[]){
    struct map tmp;
    for (int i = 0; i < MAXLEN - 1; i++){
	for (int j = 0; j < MAXLEN - 1 - i; j++){
	    if (m[j].hit < m[j+1].hit){
		tmp = m[j]; 
		m[j] = m[j+1];
		m[j+1] = tmp;
	    }
	}
    }
    
}
int overlap_key_check(FILE *fp,struct map m){
    int i = 0;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    while ((read = getline(&line, &len, fp)) != -1) {
 	    if(strncmp(line,m.key,strlen(line)-1) ==0) {
	        free(line);
	        return 1;
	    }
    }
    free(line);
    return -1;
}
void write_dnsinfo(FILE *fp,struct map m[],char rmsg[]){
    char separation[MAXLEN] = "--------------------------\n";
    int a;
    hit_sort(m);
    for(int i = 0 ; i < MAXLEN ; i ++){
	if(size_check(fp) == 0){
	    fprintf(fp,"%s\n",m[i].key);
    	    fprintf(fp,"%s",m[i].value);
      	    fprintf(fp,"hit : %d\n",m[i].hit);
	    fprintf(fp,"%s",separation);
	}else if(overlap_key_check(fp,m[i]) == 1){
	    remove("DNS_info.txt");
	    fp = fopen("DNS_info.txt","a+");
	    write_dnsinfo(fp,m,rmsg);
	    fclose(fp);
	    continue;
    	}else if(strlen(m[i].key) == 0){
	    break;
	}else if(overlap_key_check(fp,m[i]) == -1){
	    fprintf(fp,"%s\n",m[i].key);
    	    fprintf(fp,"%s",m[i].value);
            fprintf(fp,"hit : %d\n",m[i].hit);
	    fprintf(fp,"%s",separation);
        }
    }
   
}

char* info_addr(struct hostent *a,char b[])
{   
    char tmp[MAXLEN] = "";
    sprintf(b,"Official name : %s\nHost type : %s\nAddr length : %d\n",
	    a->h_name,(a->h_addrtype==AF_INET)?"AF_INET4" : "AF_INET6",a->h_length);

    while(*a->h_aliases !=NULL)
    {
        sprintf(tmp,"aliases : %s\n",inet_ntoa(*(struct in_addr *)*a->h_aliases));
        a->h_aliases++; 
	strcat(b,tmp);
    }
   
    while(*a->h_addr_list !=NULL)
    {
        sprintf(tmp,"Addr_list : %s\n",inet_ntoa(*(struct in_addr *)*a->h_addr_list));
        a->h_addr_list++;
	strcat(b,tmp);
    }   
    return b;
}


void dyna_free(struct map m[]){
    for(int i = 0; i < MAXLEN ; i++){
	free(m[i].key);
	free(m[i].value);
    }
}
int main(int argc, char **argv)
{
	int sockfd,rmsg_len;
	int i,recindex = 0;
	socklen_t addrlen;
	struct sockaddr_in addr,cliaddr;
	struct hostent    *sendmyent;
	char rmsg[MAXLEN];
	char dns_info[MAXLEN];
	struct map m[MAXLEN];
	FILE *fp;
	char ip_error[17] = "gethost....error\n";
	char domain_error[16] = "domain....error\n";
	//struct map *m = (struct map*)malloc(sizeof(struct map)*10);

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
	{
	    return 1;
	}
	for(i = 0 ; i < MAXLEN ; i++){
	    m[i].hit = 1;
	}
	memset((void*)&addr, 0x00, sizeof(addr));
    	addr.sin_family = AF_INET;
    	addr.sin_addr.s_addr = htonl(INADDR_ANY);
    	addr.sin_port = htons(PORT_NUM);

    	addrlen = sizeof(addr);
    	if(bind(sockfd, (struct sockaddr*)&addr, addrlen) == -1 ) {
    	    return 1;
   	}
	
	while(1)
	{
		fp = fopen("DNS_info.txt","a+");
		if(fp ==NULL){
		    printf("error");		
 		}
		char *s;
		char separation[MAXLEN] = "--------------------------\n";
		addrlen = sizeof(cliaddr);
		recvfrom(sockfd, (void *)rmsg, sizeof(rmsg),0,(struct sockaddr *)&cliaddr, &addrlen);	

		printf("Clinet Info : %s (%d)\n",inet_ntoa(cliaddr.sin_addr),ntohs(cliaddr.sin_port));

		if(inet_pton(AF_INET, rmsg, &(addr.sin_addr)) != 0 )
    		{
        	    memset(&addr, 0, sizeof(addr));
		    addr.sin_addr.s_addr = inet_addr(rmsg);
	            sendmyent = gethostbyaddr((char*)&addr.sin_addr, 4, AF_INET);
	            if(!sendmyent){
	                printf("%s",domain_error);
			sendto(sockfd, (void *)ip_error, sizeof(ip_error), 0, (struct sockaddr *)&cliaddr,addrlen);
			break;
		    }			
		    s = info_addr(sendmyent,dns_info);
		    setting_map(m,rmsg,s);
		    write_dnsinfo(fp,m,rmsg);
		    
		}else{
		    sendmyent = gethostbyname(rmsg);
       		    if(sendmyent == NULL)
       		    {
		        printf("%s",ip_error);
			sendto(sockfd, (void *)domain_error, sizeof(domain_error), 0, (struct sockaddr *)&cliaddr,addrlen);
			break;
		    }
		    s = info_addr(sendmyent,dns_info);
		    setting_map(m,rmsg,s);
		    for(int i = 0 ; i < MAXLEN ; i ++){
		   	if(strlen(m[i].key) !=0){
			    printf("%d\n",i);
			    printf("%s\n",m[i].value);
			}else break;
		    }
		    write_dnsinfo(fp,m,rmsg);
		    
    		}
		for(int i = 0 ; i < 10 ; i ++){
		    if(strncmp(rmsg,m[i].key,strlen(rmsg))==0){
			recindex =i;
			break;
		    }
  			
		}
	        sendto(sockfd, (void *)m[recindex].value, sizeof(m[recindex].value), 0, (struct sockaddr *)&cliaddr,addrlen);
		fclose(fp);
		
	}
	dyna_free(m);
	fclose(fp);
	return 1;
}
