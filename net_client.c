#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netdb.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#define PORT_NUM 3800
#define MAXLEN 256


int main(int argc, char **argv)
{
	int sockfd;
	struct sockaddr_in addr,recvaddr;
        //struct hostent    *recvaddr;
	char smsg[MAXLEN];
	char rmsg[MAXLEN];
	char ip_error[17] = "gethost....error\n";
	char domain_error[16] = "domain....error\n";
	
	socklen_t addrlen;
	
	if(argc !=2)
	{
		printf("Usage : %s [ipaddress]\n", argv[0]);
		return 1;
	}

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
	{
		return 1;
	}
	memset((void *)&addr, 0x00, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(argv[1]);
	addr.sin_port = htons(PORT_NUM);

	while(1)
	{
		printf("> ");
		fgets(smsg, MAXLEN,stdin);
		smsg[strlen(smsg)-1] = '\0';
		if(strncmp(smsg, "exit\n" , 5) == 0)
		{
			break;
		}
		addrlen = sizeof(addr);
		sendto(sockfd,(void *)smsg, sizeof(smsg), 0 , (struct sockaddr *)&addr, addrlen);
		recvfrom(sockfd, (void *)rmsg, sizeof(rmsg), 0 , (struct sockaddr *)&recvaddr, &addrlen);
		/*if(strncmp(rmsg,ip_error,strlen(ip_error))==0 || strncmp(rmsg,domain_error,strlen(domain_error))==0){
			printf("%s\n",rmsg);
			printf("%d\n",strlen(rmsg));
			printf("%d\n",strlen(ip_error));
			break;
		}*/
		//printf("%s",rmsg);
	}	
	close(sockfd);
	return 1;
}
