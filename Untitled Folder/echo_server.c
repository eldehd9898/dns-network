#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#define MAXBUF 1024


void reverse(char a[]) {
	int asize = strlen(a);
	char tmp;
	for (int i = asize; i >0; i--) {
		for (int j = 0; j< i-1; j++) {
			tmp = a[j];
			a[j] = a[j + 1];
			a[j + 1] = tmp;

		}
	}
}

int main(int argc, char **argv){
	int server_sockfd, client_sockfd;
	int client_len, n;
	char buf[MAXBUF];
	char logdata[256];

	FILE *fp;
	struct sockaddr_in clientaddr,serveraddr;

	client_len = sizeof(clientaddr);

	if((server_sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1){
		perror("socket error : ");
		exit(0);
	}

	bzero(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(atoi(argv[1]));
	serveraddr.sin_port = htons(atoi(argv[2]));

	bind(server_sockfd,(struct sockaddr *)&serveraddr, sizeof(serveraddr));
	listen(server_sockfd,5);

	while(1)
	{	
		time_t timer; 
		struct tm* t;
		timer = time(NULL); 
		t = localtime(&timer);
		fp = fopen("logfile.dat","a");
		memset(buf,0x00,MAXBUF);
		client_sockfd = accept(server_sockfd,(struct sockaddr*)&clientaddr,
					&client_len);
		printf("New Client Connect : %s\n", inet_ntoa(clientaddr.sin_addr));
		
				
		
		if( (n=read(client_sockfd, buf, MAXBUF)) <= 0){
			close(client_sockfd);
			continue;
		}
		reverse(buf);
		sprintf(logdata,"%s %d-%d-%d %d:%d:%d %s\n", inet_ntoa(clientaddr.sin_addr),
			t->tm_year + 1900, t->tm_mon + 1, t->tm_mday,t->tm_hour, t->tm_min, t->tm_sec,
			buf);
		fprintf(fp,"%s",logdata);

		printf("Reverse Read : %s\n", buf);
		

		if( write(client_sockfd, buf, MAXBUF) <= 0){
			perror("write error : ");
			close(client_sockfd);
		}
		close(client_sockfd);
		fclose(fp);
	
	}
	close(client_sockfd);
	
	return 0;
}
